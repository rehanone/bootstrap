#!/bin/bash

# Stop on error
set -e

sudo apt-get update
sudo apt-get install openssh-server git

mkdir /home/ray/dev
cd /home/ray/dev

git clone https://rehanone@bitbucket.org/rehanone/scripts.git

cd /home/ray/dev/scripts/common

./install_puppet_ubuntu.sh
./install_puppet_modules.sh
